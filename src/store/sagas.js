import { all, put } from 'redux-saga/effects';
import {CartActionTypes} from '../store/common/cart/types';

export function* AddToCart() {
  yield put({type: CartActionTypes.AddToCart});
}

export default function* sagas() {
  yield all([
    // All sage here
  ]);
}