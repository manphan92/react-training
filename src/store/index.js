import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

// Import your state types and reducers here.
// Language
import languageReducer from './common/language/reducer';
// Spinner
import spinnerReducer from './common/spinner/reducer';
import homeReducer from './common/home/reducer';
import cartReducer from './common/cart/reducer';
import { reducer as formReducer } from 'redux-form';
// Whenever an action is dispatched, Redux will update each top-level application state property
// using the reducer with the matching name. It's important that the names match exactly, and that
// the reducer acts on the corresponding ApplicationState property type.
const appReducers = combineReducers({
  router: routerReducer,
  language: languageReducer,
  spinner: spinnerReducer,
  home: homeReducer,
  form: formReducer,
  cart: cartReducer
});

export const reducers = (state, action) => {
  // if (action.type === AuthActionTypes.LOGGED_OUT) {
  //     state = {};
  // }

  return appReducers(state, action);
};
