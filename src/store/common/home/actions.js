import {
  HomeActionTypes
} from './types';

/**
 * Load Spinner
 */
export const FilterItem = (typeID, TypeList) => {
  return {
    type: HomeActionTypes.Filter,
    typeID: typeID,
    TypeList: TypeList
  };
};

/**
 * Hide Spinner
 */
export const AddNewItem = (titleItem,contentItem) => {
  return {
    type: HomeActionTypes.AddNew,
    titleItem: titleItem,
    contentItem: contentItem
  };
};

export const Delete = (index) => {
  return {
    type: HomeActionTypes.Delete,
    idItem: index
  };
};

export const GetDetail = (index) => {
  return {
    type: HomeActionTypes.GetDetail,
    idItem: index
  };
};
