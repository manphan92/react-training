export const HomeActionTypes = {
  Filter: '@@home/Filter',
  AddNew: '@@home/AddNew',
  Delete: '@@home/Delete',
  GetDetail: '@@home/GetDetail'
};
