/**
 * Check spinner loading
 * @param {ApplicationState} state
 * @returns {boolean}
 */

export const listItem = (state) => state.home.listItem;

export const listFilter = (state) => state.home.listFilter;

export const detailItem = (state) => state.home.detailItem;

