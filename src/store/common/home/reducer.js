import { HomeActionTypes } from './types';

let list = [];
for (let index = 0; index < 9; index++) {
  if (index < 6)
    list.push({
      titleItem: `Xe oto ${index}`,
      contentItem: `Mau xe moi, chat luong cao. Chat luong ${index} sao.`,
      id: index,
      type: 1
    });
  else
    list.push({
      titleItem: `Xe oto ${index}`,
      contentItem: `Mau xe moi, chat luong cao. Chat luong ${index} sao.`,
      id: index,
      type: 2
    });
}

export const initialState = {
  listItem: list,
  listFilter: list,
  detailItem: {}
};

const reducer = (state = initialState, action) => {
  const { type, titleItem, contentItem, idItem, typeID, TypeList } = action;
  switch (type) {

    case HomeActionTypes.Filter:
      let newList = [];
      if (TypeList == 2) {
        let listItem1 = state.listItem;
        for (let index = 0; index < listItem1.length; index++) {
          if (listItem1[index].type == typeID) {
            newList.push(listItem1[index]);
          }
        }
      }
      else
        newList = state.listItem;
      return { ...state, listFilter: newList };

    case HomeActionTypes.AddNew:
      state.listItem.push({
        titleItem: titleItem,
        contentItem: contentItem,
        id: state.listItem.length,
        type: 3
      });
      return { ...state, listFilter: state.listItem };

    case HomeActionTypes.Delete:
      for (let i = 0; i < state.listItem.length; i++) {
        if (idItem == state.listItem[i].id) {
          state.listItem.splice(i, 1);
          break;
        }
      }
      return { ...state, listFilter: state.listItem };

    case HomeActionTypes.GetDetail:
      for (let i1 = 0; i1 < state.listItem.length; i1++) {
        if (state.listItem[i1].id == idItem) {
          state.detailItem = state.listItem[i1];
          break;
        }
      }
      return { ...state, detailItem: state.detailItem };

    default:
      return state;
  }
};

export default reducer;
