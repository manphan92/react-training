import { CartActionTypes } from './types';

export const initialState = {
  totalItem: 0,
  item: []
};

const reducer = (state = initialState, action) => {
  const { type, idItem } = action;
  switch (type) {

    case CartActionTypes.AddToCart:
      state.totalItem++;
      let newItem = {
        idItem: null,
        quantity: 0
      }
      let count=0;
      if (state.item.length != 0) {
        for(let index=0;index<state.item.length;index++) {
          if (state.item[index].idItem == idItem) {
            state.item[index].idItem = idItem;
            state.item[index].quantity++;
            state.item.push(state.item[index]);
            state.item.splice(index,1);
            count++;
            break;
          }
        }
        if(count==0){
          newItem.idItem = idItem;
          newItem.quantity++;
          state.item.push(newItem);
        }
      } else {
        newItem.idItem = idItem;
        newItem.quantity++;
        state.item.push(newItem);
      }
      return { ...state, totalItem: state.totalItem, item: state.item }

    default:
      return state;
  }
};

export default reducer;
