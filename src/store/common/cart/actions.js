import {
  CartActionTypes
} from './types';

export const AddToCart = (idItem)=>{
  return{
    type: CartActionTypes.AddToCart,
    idItem: idItem
  };
};
