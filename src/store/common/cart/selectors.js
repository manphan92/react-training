/**
 * Check spinner loading
 * @param {ApplicationState} state
 * @returns {boolean}
 */
export const totalItem = (state) => state.cart.totalItem;

export const item = (state) => state.cart.item;

