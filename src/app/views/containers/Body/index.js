import React from 'react';
import './styles.scss';
import Item from '../Item';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { AddNewItem } from '../../../../store/common/home/actions';
import FormAddNew from '../../components/FormAddNew'; 

class Body extends React.Component {

    constructor(props){
        super(props);

        this.state={
            open: false,
        }

        this.handleClickOpen=this.handleClickOpen.bind(this);
        this.handleClose=this.handleClose.bind(this);
        this.addNew=this.addNew.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.submitForm=this.submitForm.bind(this);
    }

    handleClickOpen() {
        this.setState({ open: true });
    };

    handleClose() {
        this.setState({ open: false });
    };

    addNew() {
        this.props.AddNewItem(this.state.titleItemNew,this.state.contentItemNew);
        this.setState({
            open: false
        });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    submitForm(values) {
        console.log(values);
        this.props.AddNewItem(values.titleItem,values.contentItem);
        this.setState({
            open: false
        });
    }

    render() {
        return (
            <React.Fragment>
                <Button size="large" variant="contained" color="primary" className='toright' onClick={this.handleClickOpen}>
                    Them moi
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Add new car"}</DialogTitle>
                    <DialogContent>
                        {/* <Grid container spacing={16}>
                            <Grid item xs={8}>
                                <TextField
                                    id="standard-name"
                                    name="titleItemNew"
                                    label="Title"
                                    className='toright textField'
                                    value={this.state.titleItemNew}
                                    onChange={this.handleChange}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    id="standard-name"
                                    name="contentItemNew"
                                    label="Content"
                                    className='toright textField'
                                    value={this.state.contentItemNew}
                                    onChange={this.handleChange}
                                    margin="normal"
                                />
                            </Grid>
                        </Grid> */}
                        <FormAddNew onSubmit={this.submitForm}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
                
                <Grid container spacing={16}>
                    {
                        this.props.listFilter.map((item, index) => {
                            return (
                                    <Item value={item} key={index} />
                            );
                        })
                    }
                </Grid>
                </React.Fragment>
        );
    }
}

export default connect(null,{AddNewItem})(Body);