import React from 'react';
import Badge from '@material-ui/core/Badge';
import {connect} from 'react-redux';
import {totalItem,item} from 'store/common/cart/selectors';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import './styles.scss';

class Cart extends React.Component{
    constructor(props){
        super(props);
        this.state={
            open: false
        }
        this.handleClickOpen=this.handleClickOpen.bind(this);
        this.handleClose=this.handleClose.bind(this);
    }

    handleClickOpen(){
        this.setState({ open: true });
      };

    handleClose(){
        this.setState({ open: false });
      };

    render(){
        const {item, totalItem} = this.props
        return(
            <React.Fragment>
                <Badge badgeContent={totalItem} className='toright' color="primary">
                    <img src="./assets/img/cart.png" className='toright img' onClick={this.handleClickOpen}></img>
                </Badge>
                <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="draggable-dialog-title"
                >
                <DialogTitle id="draggable-dialog-title">Your cart</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    {
                        item.map((value,index)=>{
                            return(
                                <p key={index}>Ma xe: {value.idItem} | So luong: {value.quantity}</p>
                            )
                        })
                    }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                    Cancel
                    </Button>
                    <Button onClick={this.handleClose} color="primary">
                    Checkout
                    </Button>
                </DialogActions>
                </Dialog>
          </React.Fragment>
        );
    }
}

function mapStateToProps(state){
  return({
    totalItem: totalItem(state),
    item: item(state)
  });
}

export default connect(mapStateToProps)(Cart);