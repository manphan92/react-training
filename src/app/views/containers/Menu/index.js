import React from 'react';
import './styles.scss'
import PropTypes from 'prop-types';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import { FilterItem } from '../../../../store/common/home/actions';
import { connect } from 'react-redux';

class Menu extends React.Component {
  constructor(props) {
    super(props);

    //this.onClick = this.onClick.bind(this);
  }

  onClick(typeID) {
    this.props.FilterItem(typeID, 2);
  }

  render() {
    return (
      <Paper>
        <MenuList>
          {
            this.props.values.map((item,index)=>{
                return(
                  <MenuItem key={index}>
                    <ListItemText inset primary={item.typeName} onClick={this.onClick.bind(this,item.typeID)} />
                  </MenuItem>
                );
            })
          }
        </MenuList>
      </Paper>
    );
  }
}

Menu.propTypes = {
  onFilter: PropTypes.func,
  value: PropTypes.shape({
    typeID: PropTypes.number,
    typeName: PropTypes.string
  })
};

export default connect(null, { FilterItem })(Menu);