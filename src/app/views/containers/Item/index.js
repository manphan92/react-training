import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import './styles.scss';
import { Link } from "react-router-dom";
import { Delete } from '../../../../store/common/home/actions';
import { connect } from 'react-redux';

class Item extends React.Component {
  constructor(props) {
    super(props);

    this.onclick = this.onclick.bind(this);
  }

  // componentWillMount(){
  //   console.log('componentWillMount item: ' + this.props.contentItem);
  // }

  // componentDidMount(){
  //   console.log('componentDidMount item: ' + this.props.contentItem);
  // }

  // componentWillUpdate(nextProps, nextState) {
  //  console.log(`componentWillUpdate Updated: ${nextProps.contentItem}`);
  // } 

  // componentDidUpdate(prevProps,prevState){
  //   console.log(`componentDidUpdate: ${prevProps.contentItem}`);
  // }

  onclick() {
    this.props.Delete(this.props.value.id);
  }

  render() {
    return (
      <Grid item xs={3}>
        <Card>
          <CardActionArea>
            <CardMedia
              component="img"
              alt="Contemplative Reptile"
              height="140"
              image="./assets/img/car.png"
              title="Contemplative Reptile"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                <Link to={{
                  pathname: `/dashboard/detail-item/${this.props.value.id}`,
                  search: `?type=${this.props.value.type}`
                }}>
                  {this.props.value.titleItem}
                </Link>
              </Typography>
              <Typography component="p">
                {this.props.value.contentItem}
              </Typography>
              <Typography component="p">
                Loai xe: {this.props.value.type}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Link to={'/dashboard'}>
              <Button variant="contained" color="secondary" className='toright buttondelete' onClick={this.onclick}>
                X
              </Button>
            </Link>
          </CardActions>
        </Card>
      </Grid>
    );
  }
}

Item.propTypes = {
  delete: PropTypes.func,
  value: PropTypes.shape({
    titleItem: PropTypes.string,
    contentItem: PropTypes.string,
    type: PropTypes.number
  })
};

export default connect(null, { Delete })(Item);