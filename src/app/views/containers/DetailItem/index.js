import React from 'react';
import './styles.scss';
import Item from '../Item';

import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { GetDetail,FilterItem } from '../../../../store/common/home/actions';
import { AddToCart } from '../../../../store/common/cart/actions';
import { detailItem, listFilter } from 'store/common/home/selectors';
import { Paper, Button } from '@material-ui/core';

class DetailItem extends React.Component {
    constructor(props) {
        super(props);

        this.addToCart=this.addToCart.bind(this);
    }

    componentWillMount(){
        this.props.GetDetail(this.props.idItem);
        this.props.FilterItem(this.props.type, 2);
    }

    componentWillReceiveProps(nextProps){
        console.log(nextProps);
        if(nextProps.idItem!=this.props.detailItem.id){
            this.props.GetDetail(nextProps.idItem);
            this.props.FilterItem(nextProps.type, 2);
        }
    }

    addToCart(){
        this.props.AddToCart(this.props.detailItem.id);
    }

    render() {
        return (
            <React.Fragment>
                <br />
                <center><h2>Chi tiet san pham</h2></center>
                <Grid container spacing={8}>
                    <Grid item xs={6}>
                        <center><img src='./assets/img/car.png' /></center>
                    </Grid>
                    <Grid item xs={6}>
                        <p>Ten xe :{this.props.detailItem.titleItem}</p>
                        <p>Mo ta :{this.props.detailItem.contentItem}</p>
                        <p>Loai xe :{this.props.detailItem.type}</p>
                        <p>ID :{this.props.detailItem.id}</p>
                        <Button size="large" variant="contained" color="primary" onClick={this.addToCart}>Add to cart</Button>
                    </Grid>
                </Grid>

                <Paper>
                    <center><h3>San pham cung loai</h3></center>
                    <Grid container spacing={16}>
                        {
                            this.props.listFilter.map((item, index) => {
                                return (
                                    <Item value={item} key={index} />
                                );
                            })
                        }
                    </Grid>
                </Paper>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return({
      detailItem: detailItem(state),
      listFilter: listFilter(state)
    });
  }

export default connect(mapStateToProps,{ GetDetail,FilterItem, AddToCart })(DetailItem);
