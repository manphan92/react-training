import React from 'react';

import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';

import Menu from '../Menu';
import Body from '../Body';
import Filter from '../Filter';
import { FilterItem } from '../../../../store/common/home/actions';
import { listFilter } from 'store/common/home/selectors';

class HomeContainer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      titleItemNew: '',
      contentItemNew: '',
      listType: [{
        typeID: 1,
        typeName: 'Xe < 5 sao'
      },
      {
        typeID: 2,
        typeName: 'Xe > 5 sao'
      }, {
        typeID: 3,
        typeName: 'Xe moi'
      }]
    };
  }

  componentWillMount(){
    this.props.FilterItem(0, 1);
}

  render() {
    return (
        <Grid container spacing={16}>
          <Grid item xs={3}>
            <Menu values={this.state.listType} />
            <Filter values={this.state.listType} />
          </Grid>
          <Grid item xs={9}>
            <Body listFilter={this.props.listFilter} />
          </Grid>
        </Grid>
    );
  }
}

function mapStateToProps(state) {
  return({
    listFilter: listFilter(state)
  });
}

export default connect(mapStateToProps,{FilterItem})(HomeContainer);
