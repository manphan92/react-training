import React from 'react';
import './styles.scss'

import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { Grid } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { FilterItem } from '../../../../store/common/home/actions';
import { connect } from 'react-redux';

class Filter extends React.Component {
    constructor(props){
        super(props);
        this.onClick=this.onClick.bind(this);
    }

    onClick(event) {
        this.props.FilterItem(event.target.value,2);
      }

    render() {
        return (
            <Paper>
                <Grid container spacing={8}>
                    <Grid item xs={2}>
                       
                    </Grid>
                    <Grid item xs={9}>
                        <FormControl className='selectType'>
                            <InputLabel htmlFor="age-simple">Type</InputLabel>
                            <Select
                                //value={this.state.age}
                                onChange={this.onClick}
                                inputProps={{
                                    name: 'type',
                                    id: 'type-id',
                                }}
                            >
                                {
                                    this.props.values.map((item,index)=>{
                                        return(
                                            <MenuItem value={item.typeID} key={index}>{item.typeName}</MenuItem>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

Filter.propTypes={
    onFilter: PropTypes.func,
    values: PropTypes.arrayOf(PropTypes.shape({
        typeID: PropTypes.number,
        typeName: PropTypes.string
    }))
  };

export default connect(null,{FilterItem})(Filter);