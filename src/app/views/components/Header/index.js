import React from 'react';

import Typography from '@material-ui/core/Typography';
import './styles.scss';
import { Link } from "react-router-dom";
import Cart from '../../containers/Cart';

const Header = () => {

  return (
    <div>
      <div style={{ 'background': 'linear-gradient(to right, rgb(0, 0, 70), rgb(28, 181, 224))', 'height': '300px' }}>
        <Typography variant="h4" color="inherit" style={{ 'cursor': 'pointer', 'color': 'white' }}>
          <Link to={'/dashboard'}>Trang chu</Link>
        </Typography>
        <Cart />
      </div>
      <br />
    </div>
  );
};


export default Header;