import React from 'react';
import Button from '@material-ui/core/Button';
import { Field, reduxForm } from 'redux-form';
import TextField from '@material-ui/core/TextField';

const renderTextField = (
    { input, label, meta: { touched, error }, ...custom },
  ) => (
    <TextField
      hintText={label}
      floatingLabelText={label}
      errorText={touched && error}
      {...input}
      {...custom}
    />
  );

const FormAddNew = props => {
    const { handleSubmit, pristine, reset, submitting } = props;
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label>Title item</label>
                <div>
                    <Field
                        name="titleItem"
                        component={renderTextField}
                        type="text"
                    />
                </div>
            </div>
            <br />
            <div>
                <label>Content item</label>
                <div>
                    <Field
                        name="contentItem"
                        component={renderTextField}
                        type="text"
                    />
                </div>
            </div>
            <div>
                <Button onClick={reset} color="primary">
                    Reset
                </Button>
                <Button type="submit" disabled={pristine || submitting} color="primary" autoFocus>
                    Save
                </Button>
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'FormAdd', // a unique identifier for this form
})(FormAddNew);
