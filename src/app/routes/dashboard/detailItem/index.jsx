import React from 'react';

import DetailItem from '../../../views/containers/DetailItem';
import queryString from 'query-string';

class DetailItemRoutes extends React.Component {

  // componentWillMount(){
  //   console.log('url: ' + this.props.match.params.idItem);
  // }

  render() {
    return (
      <DetailItem
        idItem={this.props.match.params.idItem}
        type={queryString.parse(this.props.location.search).type!=null?queryString.parse(this.props.location.search).type:1}
      />
    );
  }
}


export default DetailItemRoutes;
