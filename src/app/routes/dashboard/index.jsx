import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router-dom';

import HomeContainer from './home';
import DetailItem from './detailItem';
import Header from '../../views/components/Header';

const DashboardRoutes = ({ match }) => {
  return (
    <React.Fragment>
      <Header />
      <Switch key={'dashboard'}>
        <Redirect exact={true} from={`${match.url}/`} to={`${match.url}/home`} />
        <Route path={`${match.url}/home`} exact={true} component={HomeContainer} />
        <Route path={`${match.url}/detail-item/:idItem`} exact={true} component={DetailItem} />
      </Switch>
    </React.Fragment>
  );
};

DashboardRoutes.propTypes = {
  match: PropTypes.object
};

export default DashboardRoutes;
